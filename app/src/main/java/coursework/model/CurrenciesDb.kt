package coursework.model

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context

@Database(entities = arrayOf(
        AvailableCurrency::class,
        ExchangeRates::class,
        OperationsHistory::class,
        HistoricalData::class), version = 2, exportSchema = false)
@TypeConverters(DataConverter::class)
abstract class CurrenciesDb : RoomDatabase() {
    abstract fun availableCurrenciesDao(): AvailableCurrenciesDao
    abstract fun exchangeRatesDao(): ExchangeRatesDao
    abstract fun operationsHistoryDao(): OperationsHistoryDao
    abstract fun historicalDataDao(): HistoricalDataDao

    companion object {

        @Volatile
        private var INSTANCE: CurrenciesDb? = null

        fun getInstance(context: Context): CurrenciesDb =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
                }

        private fun buildDatabase(context: Context) =
                Room.databaseBuilder(context.applicationContext,
                        CurrenciesDb::class.java, "Sample.db")
                        .fallbackToDestructiveMigration()
                        .build()
    }
}