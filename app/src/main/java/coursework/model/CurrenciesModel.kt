package coursework.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverter
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*
import kotlin.collections.HashMap

interface DbItem {var id : Long}

@Entity(tableName = "available_currencies")
data class AvailableCurrency(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id") override var id: Long = UUID.randomUUID().mostSignificantBits,
        @ColumnInfo(name = "name") var name: String,
        @ColumnInfo(name = "sign") var sign: String,
        @ColumnInfo(name = "is_favorite") var isFavorite: Boolean = false,

        //in case if API endpoints won't work for a specific currency,
        //we can simply turn it off for the end user
        @ColumnInfo(name = "is_available") var isAvailable: Boolean? = true,
        @ColumnInfo(name = "symbol") var symbol: String? = null,
        @ColumnInfo(name = "use_counter_as_base") var useCounterAsBase: Int = 0,
        @ColumnInfo(name = "use_counter_as_destination") var useCounterAsDestination: Int = 0,
        @ColumnInfo(name = "last_use") var lastUse: Long = 0
) : Parcelable, DbItem {
    constructor(source: Parcel) : this(
            source.readLong(),
            source.readString(),
            source.readString(),
            1 == source.readInt(),
            source.readValue(Boolean::class.java.classLoader) as Boolean?,
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int,
            source.readValue(Int::class.java.classLoader) as Int,
            source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeLong(id)
        writeString(name)
        writeString(sign)
        writeInt((if (isFavorite) 1 else 0))
        writeValue(isAvailable)
        writeString(symbol)
        writeValue(useCounterAsBase)
        writeValue(useCounterAsDestination)
        writeLong(lastUse)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<AvailableCurrency> = object : Parcelable.Creator<AvailableCurrency> {
            override fun createFromParcel(source: Parcel): AvailableCurrency = AvailableCurrency(source)
            override fun newArray(size: Int): Array<AvailableCurrency?> = arrayOfNulls(size)
        }
    }
}

@Entity(tableName = "exchange_rates")
data class ExchangeRates(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id") var id: Long? = UUID.randomUUID().mostSignificantBits,
        @ColumnInfo(name = "timestamp_server") var timestamp_server: Long,
        @ColumnInfo(name = "timestamp_fetched") var timestamp_fetched: Long,
        @ColumnInfo(name = "base") var base: String,
        @ColumnInfo(name = "date") var date: String,
        @ColumnInfo(name = "rates") var rates: HashMap<String, Double>
)

@Entity(tableName = "operations_history")
data class OperationsHistory(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id") var id: Long? = UUID.randomUUID().mostSignificantBits,
        @ColumnInfo(name = "base") var base: String,
        @ColumnInfo(name = "destination") var destination: String,
        @ColumnInfo(name = "base_amount") var baseAmount: Double,
        @ColumnInfo(name = "destination_amount") var destinationAmount: Double,
        @ColumnInfo(name = "timestamp") var timestamp: Long
)

class Currencies() {
    var name: String? = null
    var symbol: String? = null
}

@Entity(tableName = "historical_data")
data class HistoricalData(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name="id") var id: Long? = UUID.randomUUID().mostSignificantBits,
        @ColumnInfo(name="date")var date: String,
        @ColumnInfo(name="base")var base: String,
        @ColumnInfo(name="timestamp") var timestamp: String,
        @ColumnInfo(name = "rates") var rates: HashMap<String, Double>
)

class DataConverter {
    @TypeConverter
    fun fromNodeList(rates: HashMap<String, Double>): String? {
        val gson = Gson()
        val type = object : TypeToken<HashMap<String, Double>>() {}.type
        return gson.toJson(rates, type)
    }

    @TypeConverter
    fun toNodeList(rates: String?): HashMap<String, Double>? {
        val gson = Gson()
        val type = object : TypeToken<HashMap<String, Double>>() {}.type
        return gson.fromJson<HashMap<String, Double>>(rates, type)
    }
}

