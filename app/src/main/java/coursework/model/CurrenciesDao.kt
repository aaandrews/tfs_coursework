package coursework.model

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface AvailableCurrenciesDao {
    @Query("select * from available_currencies ORDER BY is_favorite DESC, last_use DESC, sign ASC")
    fun getAllAvailableCurrencies(): Flowable<List<AvailableCurrency>>

    @Query("select * from available_currencies where is_favorite = 1")
    fun getAllFavorites(): Flowable<List<AvailableCurrency>>

    @Query("select * from available_currencies where sign = :sign")
    fun getCurrencyBySign(sign: String): AvailableCurrency

    @Query("select * from available_currencies where id = :id")
    fun getCurrencyById(id: Long) : Single<AvailableCurrency>

    @Insert(onConflict = REPLACE)
    fun insertAvailableCurrency(currency: AvailableCurrency): Long

    @Insert(onConflict = REPLACE)
    fun insertAvailableCurrencies(vararg currencies: AvailableCurrency) : List<Long>

    @Delete
    fun removeAvailableCurrency(currency: AvailableCurrency)

    @Query("DELETE from available_currencies")
    fun removeAllAvailableCurrencies()
}

@Dao
interface ExchangeRatesDao {
    @Query("select * from exchange_rates")
    fun getExchangeRates(): Single<List<ExchangeRates>>

    @Insert(onConflict = REPLACE)
    fun insertExchangeRate(rate: ExchangeRates): Long

    @Delete
    fun removeExchangeRate(rate: ExchangeRates)

    @Query("DELETE from exchange_rates")
    fun removeExchangeRates()
}

@Dao
interface OperationsHistoryDao {
    @Query("select * from operations_history")
    fun getAllOperationsHistory(): Flowable<List<OperationsHistory>>

    @Insert(onConflict = REPLACE)
    fun insertOperationsHistory(operation: OperationsHistory): Long

    @Insert(onConflict = REPLACE)
    fun insertAllOperationsHistory(operations: List<OperationsHistory>): List<Long>

    @Delete
    fun removeOperationHistory(operation: OperationsHistory)

    @Query("DELETE from operations_history")
    fun removeAllOperationsHistory()
}

@Dao
interface HistoricalDataDao {
    @Query("select * from historical_data")
    fun getAllHistoricalData() : Single<List<HistoricalData>>

    @Query("select * from historical_data where date = :date")
    fun getHistoricalDataByDate(date: String) : Single<List<HistoricalData>>

    @Insert(onConflict = REPLACE)
    fun insertHistoricalData(data: HistoricalData)

    @Query("DELETE from historical_data")
    fun removeAllHistoricalData()

    @Query("DELETE FROM historical_data WHERE date = :date")
    fun removeByDate(date: String)
}