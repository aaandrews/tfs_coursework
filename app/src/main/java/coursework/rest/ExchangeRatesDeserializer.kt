package coursework.rest

import com.google.gson.*
import coursework.model.Currencies
import coursework.model.ExchangeRates
import java.lang.reflect.Type
import java.util.*
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit


class ExchangeRatesDeserializer : JsonDeserializer<ExchangeRates> {

    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): ExchangeRates {
        val exchangeRates = ExchangeRates(0, 0, 0, "", "", hashMapOf())

        if (json.isJsonObject) {
            val jsonObject = json.asJsonObject

            if (jsonObject.size() > 0) {
                val timestamp = jsonObject.get("timestamp")
                if (timestamp != null) {
                    exchangeRates.timestamp_server = timestamp.asLong
                    exchangeRates.timestamp_fetched = System.currentTimeMillis() / 1000
                } else throw IllegalStateException()
                val base = jsonObject.get("base")
                if (base != null) {
                    exchangeRates.base = base.asString
                } else throw IllegalStateException()
                val date = jsonObject.get("date")
                if (date != null && date.isJsonPrimitive) {
                    exchangeRates.date = date.asString
                } else throw IllegalStateException()
                val rates = jsonObject.get("rates")
                if (rates != null && rates.isJsonObject) {
                    val keys = rates.asJsonObject
                            .run { entrySet() }
                    for (entry in keys) {
                        exchangeRates.rates[entry.key] = entry.value.asDouble
                    }
                } else throw IllegalStateException()
            }
        }
        return exchangeRates
    }
}

class CurrencyNamesDeserializer : JsonDeserializer<List<Currencies>> {
    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): List<Currencies> {
        val list: ArrayList<Currencies> = arrayListOf()

        if (json.isJsonObject) {
            val jsonObject = json.asJsonObject
            if (jsonObject.size() > 0) {
                val symbols = jsonObject.get("symbols")
                if (symbols != null && symbols.isJsonObject) {
                    val keys = symbols.asJsonObject.run { entrySet() }
                    for (entry in keys) {
                        //Log.d("Currencies deserializer", "${entry.key} _ ${entry.value}")
                        val c = Currencies()
                        c.symbol = entry.key.toString()
                        c.name = entry.value.toString().replace("\"", "")
                        list.add(c)
                    }
                }else throw IllegalStateException()

            } else throw IllegalStateException()
        }
        return list
    }
}

open class NullOnEmptyConverterFactory : Converter.Factory() {

    override fun responseBodyConverter(type: Type?, annotations: Array<Annotation>?, retrofit: Retrofit?): Converter<ResponseBody, *>? {
        val delegate = retrofit!!.nextResponseBodyConverter<Any>(this, type!!, annotations!!)
        return Converter<ResponseBody, Any> { body -> if (body.contentLength() == 0L) null else delegate.convert(body) }
    }
}