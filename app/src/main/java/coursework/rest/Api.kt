package coursework.rest

import io.reactivex.Flowable
import io.reactivex.Single
import coursework.model.*
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface CurrencyApi {
    @GET("latest")
    fun getLatestRates(@Query("access_key") apiKey : String) : Flowable<ExchangeRates>

    @GET("{date}")
    fun getHistoricalData(@Path("date")date: String,
                           @Query("access_key") apiKey : String,
                           @Query("base") base: String,
                           @Query("symbols") symbols: String) : Single<HistoricalData>

    @GET("symbols")
    fun getAvailableCurrencies(@Query("access_key") apiKey: String) : Single<List<Currencies>>

}