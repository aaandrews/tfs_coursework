package coursework.rest

import retrofit2.Retrofit
import io.reactivex.schedulers.Schedulers
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.google.gson.GsonBuilder
import coursework.model.ExchangeRates
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.converter.gson.GsonConverterFactory


class ApiBuilder {
    companion object {
        private const val BASE_URL: String = "http://data.fixer.io/api/"
        //brand new key
        //somewhere around 1.2K request available

        const val API_KEY: String = "8b151a3f839f7f2c37aab1cf15835268"

        @Volatile
        private var INSTANCE: CurrencyApi? = null

        fun getInstance(): CurrencyApi =
                INSTANCE ?: synchronized(this) {
                    INSTANCE
                            ?: buildApi().also { INSTANCE = it }
                }

        private fun buildApi(): CurrencyApi {

            val okHttpClient = HttpLoggingInterceptor()
                    .apply { level = HttpLoggingInterceptor.Level.BASIC }
                    .let {
                        OkHttpClient().newBuilder().addInterceptor(it).build()
                    }


            val gson = GsonBuilder()
                    .registerTypeAdapter(ExchangeRates::class.java, ExchangeRatesDeserializer())
                    .registerTypeAdapter(List::class.java, CurrencyNamesDeserializer())
                    .create()

            val rxAdapter = RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io())
            val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(NullOnEmptyConverterFactory())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(rxAdapter)
                    .client(okHttpClient)
                    .build()

            return retrofit.create<CurrencyApi>(CurrencyApi::class.java)
        }
    }
}