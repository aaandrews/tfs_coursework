package coursework.Repository

import android.util.Log
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import coursework.model.*
import coursework.rest.ApiBuilder
import java.util.*
import javax.inject.Inject

class CurrencyRepository @Inject constructor(
        private var availableCurrenciesDao: AvailableCurrenciesDao,
        private var exchangeRatesDao: ExchangeRatesDao,
        private var operationsHistoryDao: OperationsHistoryDao,
        private var historicalDao: HistoricalDataDao
) {

    private val TAG: String = this.javaClass.simpleName

    companion object {
        const val EXCHANGE_RATES_CACHE_TIME = 300 //300s 5min
    }

    fun getAvailableCurrencies(): Flowable<List<AvailableCurrency>> {
        //decide which data source to use -> cache or remote api
        //for now user remote api
        return availableCurrenciesDao.getAllAvailableCurrencies()
                .flatMap {
                    if (it.isEmpty()) {
                        exchangeRatesToAvailableCurrencies().toFlowable()
                    } else Flowable.fromCallable { it }
                }
    }

    private fun exchangeRatesToAvailableCurrencies(): Single<List<AvailableCurrency>> {
        return ApiBuilder.getInstance().getAvailableCurrencies(ApiBuilder.API_KEY)
                .map {
                    val list: ArrayList<AvailableCurrency> = arrayListOf()
                    var availableCurrency: AvailableCurrency?

                    for (c in it) {
                        availableCurrency = AvailableCurrency(0, "", "", false)
                        availableCurrency.name = c.name!!
                        availableCurrency.sign = c.symbol!!
                        list.add(availableCurrency)
                    }
                    availableCurrenciesDao
                            .insertAvailableCurrencies(*list.toTypedArray())
                    list
                }

    }

    fun setFavorite(id: Long, fav: Boolean) {
        availableCurrenciesDao.getCurrencyById(id)
                .subscribeOn(Schedulers.io())
                .subscribe { it ->
                    run {
                        it.isFavorite = fav
                        availableCurrenciesDao.insertAvailableCurrency(it)
                    }
                }
    }

    fun exchangeRates(): Flowable<ExchangeRates> {

        //try to get exchangerates from the db
        //if zero or expired -> fetch the remote
        //store to the cache
        //return to the original caller
        return exchangeRatesDao.getExchangeRates()
                .map {
                    if (it.isEmpty()) {
                        fetchStoreRates(true)
                    } else it[0]
                }
                .map {
                    val now = System.currentTimeMillis() / 1000
                    val diff = (now - it.timestamp_fetched)
                    if (diff > EXCHANGE_RATES_CACHE_TIME) {
                        fetchStoreRates()
                    } else it
                }.toFlowable()
    }

    private fun fetchStoreRates(empty: Boolean = false): ExchangeRates {
        return ApiBuilder.getInstance()
                .getLatestRates(ApiBuilder.API_KEY)
                .doOnNext {
                    exchangeRatesDao.insertExchangeRate(it)
                }.blockingFirst()
    }

    fun writeToHistory(history: OperationsHistory) {
        Single.just(1)
                .subscribeOn(Schedulers.io())
                .subscribe({ operationsHistoryDao.insertOperationsHistory(history) }, {})
    }

    fun getAllHistory(): Flowable<List<OperationsHistory>> {
        return operationsHistoryDao.getAllOperationsHistory()
    }

    fun getHistoricalDataByDate(date: String, curr_one: String, curr_two: String? = null): Single<HistoricalData> {
        RxJavaPlugins.setErrorHandler({})
        return historicalDao.getHistoricalDataByDate(date)
                .flatMap {
                    val curr = curr_two ?: curr_one
//
                    Log.d(TAG, "historical from db: ${it}")
                    if (it.isNotEmpty() &&
                            it[0].rates.contains(curr_one) &&
                            it[0].rates.containsKey(curr)) {
                        Single.fromCallable { it[0] }
                    } else {
                        // for the sake of simplicity request both currencies
                        val old = if (it.isEmpty()) null else it[0]
                        requestHistoricalData(date, listOfNotNull(curr_one, curr_two), old)
                    }
                }
                .doOnError { requestHistoricalData(date, listOfNotNull(curr_one, curr_two), null) }

    }

    private fun requestHistoricalData(date: String, currencies: List<String>, oldObject: HistoricalData?): Single<HistoricalData> {
        return ApiBuilder.getInstance()
                //base currency is hardcoded due to free fixer.io tier
                .getHistoricalData(date, ApiBuilder.API_KEY, "EUR", currencies.joinToString(separator = ","))
                .doOnError {
                    Flowable.empty<HistoricalData>()
                }
                .flatMap {
                    Log.d(TAG, "data from remote: $it")
                    if (it != null && it?.rates != null) {
                        writeHistoricalData(oldObject, it)
                    }
                    Single.fromCallable { it }
                }
    }

    private fun writeHistoricalData(oldObject: HistoricalData?,
                                    newObject: HistoricalData): Single<HistoricalData> {
        oldObject?.rates?.let {
            newObject.id = oldObject.id
            newObject.rates.putAll(it)
        }
        Log.d(TAG, "merged historical: $newObject")
        historicalDao.insertHistoricalData(newObject)
        return Single.fromCallable { newObject }
    }

    fun insertAvailableCurrency(currency: AvailableCurrency) {
        availableCurrenciesDao.insertAvailableCurrency(currency)
    }

    fun getAvailableCurrencyBySign(sign: String): AvailableCurrency {
        return availableCurrenciesDao.getCurrencyBySign(sign)
    }

    fun getHistoricalData() : Single<List<HistoricalData>> {
        return historicalDao.getAllHistoricalData()
    }

    fun removeHistoricalData(date: String) {
        historicalDao.removeByDate(date)
    }
}
