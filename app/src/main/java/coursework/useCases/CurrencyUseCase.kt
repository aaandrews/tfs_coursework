package coursework.useCases

import coursework.Repository.CurrencyRepository
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import coursework.model.AvailableCurrency
import coursework.model.ExchangeRates
import coursework.model.OperationsHistory
import javax.inject.Inject

class CurrencyUseCase @Inject constructor(private var repository: CurrencyRepository) {

    private val TAG: String = this.javaClass.simpleName

    fun loadCurrencies() : Flowable<List<AvailableCurrency>> = repository.getAvailableCurrencies()

    fun setFavorite(id: Long, fav: Boolean) {
        repository.setFavorite(id, fav)
    }

    fun exchangeRate(): Flowable<ExchangeRates> = repository.exchangeRates()

    fun writeOperationHistory(base:String, dest: String, baseAmount: Double, destAmount : Double) {
        val timestamp = System.currentTimeMillis() / 1000
        val operation = OperationsHistory(null, base, dest, baseAmount, destAmount, timestamp)

        repository.writeToHistory(operation)
        Flowable.fromIterable(arrayListOf(base, dest))
                .subscribeOn(Schedulers.io())
                .map{
                    val currency = repository.getAvailableCurrencyBySign(it)
                    currency.lastUse = timestamp

                    if(currency.sign == base) currency.useCounterAsBase += 1
                    else currency.useCounterAsDestination += 1
                    repository.insertAvailableCurrency(currency)
                }.subscribe()
    }

}