package coursework.useCases

import coursework.Repository.CurrencyRepository
import io.reactivex.Flowable
import coursework.model.OperationsHistory
import javax.inject.Inject

class HistoryUseCase @Inject constructor(var repository: CurrencyRepository) {

    fun allHistory() : Flowable<List<OperationsHistory>> {
        return repository.getAllHistory()
    }
}