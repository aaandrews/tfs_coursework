package coursework.useCases

import coursework.Repository.CurrencyRepository
import android.util.Log
import io.reactivex.*
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import coursework.model.AvailableCurrency
import coursework.model.HistoricalData
import coursework.viewModel.Filter
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class ChartUseCase @Inject constructor(private var repository: CurrencyRepository) {

    private var TAG: String = this.javaClass.simpleName


    fun loadHistoricalRange(curr_one: String, curr_two: String? = null, range: Filter): Flowable<List<HistoricalData>> {
        val cal = Calendar.getInstance()
        val now = cal.get(Calendar.DAY_OF_MONTH)
        when (range) {
            Filter.MONTH -> cal.add(Calendar.MONTH, -1)
            Filter.WEEK -> cal.add(Calendar.WEEK_OF_MONTH, -1)
            Filter.TWO_WEEKS -> cal.add(Calendar.WEEK_OF_MONTH, -2)
            else -> {
            }
        }
        val list: ArrayList<String> = arrayListOf()

        val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.UK)
        Log.d(TAG, "from date: ${formatter.format(cal.time)}")
        do {
            list.add(formatter.format(cal.time))
            cal.add(Calendar.DAY_OF_MONTH, 1)
        } while (cal.get(Calendar.DAY_OF_MONTH) != now)

        Log.d(TAG, "before list: ${list.size}")
        return Flowable.fromIterable(list)
                .flatMap {
                    repository.getHistoricalDataByDate(it, curr_one, curr_two)
                            .toFlowable()

                }
                .buffer(list.size)
    }

    fun loadCurrencies(): Flowable<List<AvailableCurrency>> = repository.getAvailableCurrencies()

    fun removeOldHistoricalData() {
        val cal = Calendar.getInstance()
        cal.add(Calendar.MONTH, -1)
        val timestamp = cal.time.time / 1000

        repository.getHistoricalData()
                .subscribeOn(Schedulers.io())
                .toObservable()
                .flatMapIterable { it -> it }
                .flatMap {
                    var t = it.timestamp.toLong()
//                    Log.d(TAG, "timestamp: ${timestamp} | then: ${t}" +
//                            "  | ${timestamp >= t}")
                    if (timestamp >= t) {
                        repository.removeHistoricalData(it.date)
                    }
                    Observable.fromCallable { it }
                }.subscribe()
    }
}