package coursework.Utils

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat
import java.text.SimpleDateFormat
import java.util.*

fun fetchColor(context: Context, @ColorRes color: Int): Int {
    return ContextCompat.getColor(context, color)
}

fun fetchDrawable(context: Context, drawable: Int): Drawable? {
    return ContextCompat.getDrawable(context, drawable)
}

fun dateToCalendar(timestamp: Long): Calendar {

    return Calendar.getInstance().apply { time = timestampToDate(timestamp) }
}

fun timestampToDate(timestamp: Long): Date {
    return Date().apply { time = timestamp }
}

fun stringToTimestamp(str: String, pattern: String? = null, dayEnd: Boolean = false): Long {
    val formatter = SimpleDateFormat(pattern ?: "dd/MM/yyyy", Locale.UK)
    var date = formatter.parse(str)
    //sets current day timestamp to the very maximum
    //something like 11.59:59PM
    if (dayEnd) {
        val c = Calendar.getInstance().apply { time = date }
        c.set(Calendar.SECOND, 59)
        c.set(Calendar.MINUTE, 59)
        c.set(Calendar.HOUR_OF_DAY, 23)
        date = c.time
    } else {
        //sets current day timestamp to the very minimum
        val c = Calendar.getInstance().apply { time = date }
        c.set(Calendar.MILLISECOND, 1)
        c.set(Calendar.SECOND, 1)
        c.set(Calendar.MINUTE, 1)
        c.set(Calendar.HOUR_OF_DAY, 1)
        date = c.time
    }
    return date.time
}

fun timestampToDateString(timestamp: Long, pattern: String? = null): String {
    val date = Date().apply { time = timestamp }
    return SimpleDateFormat(pattern ?: "dd/MM/yyyy", Locale.UK).format(date)
}

fun roundToThree(n: Double) : Double{
    //Java/Kotlin compiler converts any value  >= 10 million
    //  to scientific notation
    // we can avoid this, but let's stick with a default behaviour for now
    return Math.round(n*1000.0)/1000.0
}

fun Double.format(digits: Int): String = java.lang.String.format("%.${digits}f", this)!!

fun String.takeAndAppend(n: Int, str: CharSequence): String {
    require(n >= 0) { "Requested character count $n is less than zero." }
    if(length >= n) return substring(0, n.coerceAtMost(length)) + str
    return substring(0, n.coerceAtMost(length))
}
