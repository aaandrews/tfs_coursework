package coursework.view

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import coursework.adapters.ViewpagerAdapter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.ViewPager
import kotlinx.android.synthetic.main.activity_main.*
import coursework.adapters.BottomNavigationViewBehavior
import android.support.design.widget.CoordinatorLayout
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.util.Log
import android.view.*
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject
import kotlinx.android.synthetic.main.activity_main_debug.*
import java.util.*
import android.support.design.widget.Snackbar


class MainActivity : AppCompatActivity(), HasSupportFragmentInjector, SettingsFragment.DebugSettings {

    private val TAG: String = this.javaClass.simpleName
    var prevMenuItem: MenuItem? = null
    private var navBarBehaviour: BottomNavigationViewBehavior? = null
    private lateinit var pagerAdapter: ViewpagerAdapter
    private var fragments: ArrayList<AbstractFragment> = arrayListOf()
    private var idToPositionSet: HashMap<Int, Int> = hashMapOf()
    private var tabPrevPosition: Int = 0
    private var networkReceiver: NetworkConnection? = null
    private var snackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        //dagger
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        //build the main view base on the build type
        val inflater = LayoutInflater.from(this)
        if (BuildConfig.DEBUG) {
            setContentView(R.layout.activity_main_debug)
            val main = inflater.inflate(R.layout.activity_main,
                    drawer_layout, false)
            debug_content_frame.addView(main)
        } else {
            setContentView(R.layout.activity_main)
        }


        setSupportActionBar(toolbar)

        val layoutParams = bottom_navigation
                .layoutParams as CoordinatorLayout.LayoutParams
        navBarBehaviour = BottomNavigationViewBehavior(this)
        layoutParams.behavior = navBarBehaviour

        pagerAdapter = ViewpagerAdapter(supportFragmentManager)

        fragments.addAll(listOf(
                ExchangeFragment.newInstance(),
                HistoryFragment.newInstance(),
                ChartFragment.newInstance()))

        pagerAdapter.addFragments(fragments)
        viewpager.offscreenPageLimit = 3
        viewpager.adapter = pagerAdapter
        supportActionBar?.title = pagerAdapter.getPageTitle(0)

        //or iterate through the menu items and then add
        idToPositionSet[R.id.bottom_nav_exchange] = 0
        idToPositionSet[R.id.bottom_nav_history] = 1
        idToPositionSet[R.id.bottom_nav_chart] = 2

        bottom_navigation.setOnNavigationItemSelectedListener {
            val position = getDrawerPosById(it.itemId)
            viewpager.currentItem = position
            true
        }

        val actionbar = supportActionBar

        viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) = Unit

            override fun onPageScrolled(position: Int,
                                        positionOffset: Float,
                                        positionOffsetPixels: Int) = Unit

            override fun onPageSelected(position: Int) {
                navBarBehaviour?.slideUp()
                prevMenuItem?.let {
                    prevMenuItem?.setChecked(false)
                } ?: run {
                    bottom_navigation.menu.getItem(0).setChecked(true)
                }

                prevMenuItem = bottom_navigation.menu.getItem(position)
                bottom_navigation.menu.getItem(position).isChecked = true
                navigationSelected(position)

                tabPrevPosition = position
            }
        })

        if (BuildConfig.DEBUG) {
            actionbar?.setDisplayHomeAsUpEnabled(true)
            actionbar?.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp)

            val settings = SettingsFragment.newInstance()

            fragments.add(settings)
            pagerAdapter.addFragment(settings)

            bottom_navigation.menu.add(0, 1784672343, pagerAdapter.count + 1,
                    settings.getTitle())
            bottom_navigation.menu.getItem(pagerAdapter.count - 1)
                    .setIcon(R.drawable.ic_settings_white_18dp)

            nav_view.menu.add(0, 1784672343, pagerAdapter.count + 1,
                    settings.getTitle())
            nav_view.menu.getItem(pagerAdapter.count - 1)
                    .setIcon(R.drawable.ic_settings_white_18dp)
            idToPositionSet[1784672343] = pagerAdapter.count - 1


            nav_view?.setNavigationItemSelectedListener {
                prevMenuItem?.run {
                    prevMenuItem?.isChecked = false
                } ?: run {
                    nav_view.menu.getItem(0).setChecked(true)
                }
                val pos = getDrawerPosById(it.itemId)
                prevMenuItem = nav_view.menu.getItem(pos)
                nav_view.menu.getItem(pos).isChecked = true

                navigationSelected(pos)
                viewpager.currentItem = getDrawerPosById(it.itemId)
                drawer_layout.closeDrawer(Gravity.START, true)
                true
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                drawer_layout.openDrawer(GravityCompat.START, true)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun navigationSelected(position: Int) {
        supportActionBar?.title = pagerAdapter.getPageTitle(position)
        val selected = pagerAdapter.getFragment(position)
        val previous = pagerAdapter.getFragment(pagerAdapter.prevFragment)
        Log.d(TAG, "PRE: $selected | pos: $position | prevFragment: ${pagerAdapter.prevFragment}")
        with(selected) {
            //on config change onGainFocus called before onCreate
            //so we need to check if fragment is resumed
            //in order to avoid uninitialized variables
            if(isResumed) onGainFocus()
        }

        previous.onLoseFocus()
        pagerAdapter.prevFragment = position
        Log.d(TAG, "POST: $selected | pos: $position | prevFragment: ${pagerAdapter.prevFragment}")
    }

    private fun getDrawerPosById(id: Int): Int {
        return idToPositionSet[id] ?: 0
    }

    override fun onBackPressed() {
        var handled = false
        for (f in fragments) {
            handled = f.onBackPressed()
            if (handled) break
        }
        if (!handled) {
            if (tabPrevPosition != 0) {
                viewpager.currentItem = 0
            } else {
                super.onBackPressed()
            }
        }
    }

    override fun toggleBottomNav(flag: Boolean) {
        bottom_navigation.isEnabled = flag
        if (flag) {
            bottom_navigation.visibility = View.VISIBLE
        } else {
            bottom_navigation.visibility = View.GONE
        }

    }

    override fun toggleDrawer(flag: Boolean) {
        nav_view.isEnabled = flag
        if (flag) {
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            //actionbar?.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp)
        } else {
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
        }
    }

    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector()
            : AndroidInjector<Fragment>? = fragmentDispatchingAndroidInjector

    override fun onResume() {
        super.onResume()
        val inF = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        networkReceiver = NetworkConnection()
        registerReceiver(networkReceiver, inF)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(networkReceiver)
    }

    fun showInternetConnection(flag: Boolean) {
        if (flag) {
            snackbar?.dismiss()
        } else {
            snackbar = Snackbar
                    .make(bottom_navigation, "No internet connection!", Snackbar.LENGTH_LONG)
                    .setAction("CLOSE") {}
                    .setDuration(Snackbar.LENGTH_INDEFINITE)
            snackbar?.show()
        }

    }

    //broadcast receiver
    inner class NetworkConnection : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val cm: ConnectivityManager? = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            cm?.let {
                val ni: NetworkInfo? = cm.activeNetworkInfo
                showInternetConnection(ni?.isConnected ?: false)
            }
        }
    }
}