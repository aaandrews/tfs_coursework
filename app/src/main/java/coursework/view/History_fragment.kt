package coursework.view

import coursework.Utils.timestampToDateString
import coursework.adapters.recyclers.HistoryRecyclerAdapter
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_history.*
import coursework.model.OperationsHistory
import coursework.viewModel.*
import java.util.*
import javax.inject.Inject


class HistoryFragment : AbstractFragment() {

    private val TAG: String = this.javaClass.simpleName
    private lateinit var recyclerAdapter: HistoryRecyclerAdapter
    private lateinit var historyViewModel: HistoryViewModel
    @Inject
    lateinit var historyViewModelFactory: HistoryViewModelFactory

    companion object {
        fun newInstance() = HistoryFragment()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        TITLE = activity?.getString(R.string.history)?.toUpperCase() ?: super.TITLE
        historyViewModel = ViewModelProviders.of(this,
                historyViewModelFactory).get(HistoryViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        historyViewModel.history().observe(this, Observer { processHistory(it!!) })
        return inflater.inflate(R.layout.fragment_history, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerAdapter = HistoryRecyclerAdapter()
        val adapter = recyclerAdapter
        history_recycler.adapter = adapter
        history_recycler.layoutManager = LinearLayoutManager(context,
                LinearLayoutManager.VERTICAL, false)

    }

    override fun onResume() {
        super.onResume()
        historyViewModel.loadHistory()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        activity?.menuInflater?.inflate(R.menu.history_toolbar, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        when (id) {
            R.id.action_history_filter -> {
                historyViewModel.openFilters(this.context!!)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun processHistory(data: Response<List<OperationsHistory>>) {
        when (data.status) {
            Status.LOADING -> {
                history_error.visibility = View.GONE
                history_error_text.visibility = View.GONE
                history_empty.visibility = View.GONE
                history_empty_text.visibility = View.GONE
                filtered_by.visibility = View.GONE
                empty_filter.visibility = View.GONE

                history_progress.visibility = View.VISIBLE
                setHasOptionsMenu(false)
            }
            Status.SUCCESS -> {

                showSuccess()

                //if data is zero -> no history
                //id data not zero && filtered is zero -> history, empty filter
                var filtered = data.data!!
                if (data.data.isEmpty()) {
                    showEmptyHistory()
                    setHasOptionsMenu(false)
                } else {
                    setHasOptionsMenu(true)
                    filtered = filterHistory(data.data)

                    if (filtered.isEmpty()) empty_filter.visibility = View.VISIBLE
                    else empty_filter.visibility = View.GONE
                }


                recyclerAdapter.addItems(filtered, true)
            }
            Status.ERROR -> {
                showError()
                setHasOptionsMenu(false)
            }
        }
    }

    private fun showError() {
        filtered_by.visibility = View.GONE
        history_error.visibility = View.VISIBLE
        history_error_text.visibility = View.VISIBLE
        history_empty.visibility = View.GONE
        history_empty_text.visibility = View.GONE

        history_progress.visibility = View.GONE
    }

    private fun showSuccess() {
        filtered_by.visibility = View.VISIBLE
        history_error.visibility = View.GONE
        history_error_text.visibility = View.GONE


        history_progress.visibility = View.GONE
        empty_filter.visibility = View.GONE
    }

    private fun showEmptyHistory() {
        filtered_by.visibility = View.GONE
        history_empty.visibility = View.VISIBLE
        history_empty_text.visibility = View.VISIBLE
    }

    private fun filterHistory(list: List<OperationsHistory>): List<OperationsHistory> {
        var filterText = getString(R.string.filtered_by, "All Time")
        var resultList: List<OperationsHistory>
        val prefs = activity?.getSharedPreferences("FILTERS", Context.MODE_PRIVATE)
        //first filter out all non selected currencies
        val selections = prefs?.getStringSet("FILTERED_CURRENCIES", hashSetOf())!!
        resultList = list.filter {
            selections.contains(it.base) || selections.contains(it.destination)
        }

        val filter = prefs.getInt("APPLIED_FILTER", 0) //default: all_time
        val date = Date()
        var c = Calendar.getInstance().apply { time = date }
        when (filter) {
            Filter.WEEK.ordinal -> {
                val end = c.time.time
                c.add(Calendar.DATE, -7)
                val start = c.time.time
                resultList = resultList.filter { (it.timestamp * 1000) in start..end }
                filterText = getString(R.string.filtered_by, "Last Week")
            }
            Filter.MONTH.ordinal -> {
                c = Calendar.getInstance().apply { time = date }
                val end = c.time.time
                c.add(Calendar.MONTH, -1)
                val start = c.time.time
                resultList = resultList.filter { (it.timestamp * 1000) in start..end }
                filterText = getString(R.string.filtered_by, "Last Month")
            }
            Filter.RANGE.ordinal -> {
                val start = prefs.getLong("DATE_FROM", 0)
                val end = prefs.getLong("DATE_TO", 0)
                resultList = resultList.filter { (it.timestamp * 1000) in start..end }
                filterText = getString(R.string.filtered_by, "Range [ ${timestampToDateString(start)}  -  " +
                        "${timestampToDateString(end)} ]")
            }
        }
        filtered_by.text = filterText
        return resultList
    }
}
