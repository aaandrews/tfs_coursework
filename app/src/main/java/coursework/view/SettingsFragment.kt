package coursework.view


import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main_debug.*
import kotlinx.android.synthetic.main.fragment_settings_.*

class SettingsFragment : AbstractFragment() {

    private val TAG: String = this.javaClass.simpleName
    private lateinit var listener: DebugSettings
    override var TITLE = "Debug"
    companion object {
        fun newInstance() = SettingsFragment()
    }

    interface DebugSettings {
        fun toggleBottomNav(flag: Boolean)
        fun toggleDrawer(flag: Boolean)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        listener = activity as DebugSettings
        Log.d(TAG, "onCreate: $bottom_navigation")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        Log.d(TAG, "viewCreate: $bottom_navigation")
        return inflater.inflate(R.layout.fragment_settings_, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Log.d(TAG, "viewCreated: $bottom_navigation")
        toggleBottomNav()
        toggleDrawer()
    }

    override fun onStart() {
        super.onStart()

        Log.d(TAG, "start: $bottom_navigation")
        debug_bottom_nav.setOnCheckedChangeListener { buttonView, isChecked ->
            toggleBottomNav(isChecked)
        }

        debug_drawer.setOnCheckedChangeListener { buttonView, isChecked ->
            toggleDrawer(isChecked)
        }
    }

    private fun toggleBottomNav(flag: Boolean? = null) {
        var isChecked: Boolean? = flag
        val prefs = activity?.getSharedPreferences("DEBUG_SETTINGS",
                Context.MODE_PRIVATE)

        if (flag == null) {
            isChecked = prefs?.getBoolean("BOTTOM_NAV", true)
        } else {
            with(prefs?.edit()) {
                this?.putBoolean("BOTTOM_NAV", isChecked!!)
                this?.apply()
            }
        }
        listener.toggleBottomNav(isChecked!!)
        debug_bottom_nav.isChecked = isChecked
    }

    private fun toggleDrawer(flag: Boolean? = null) {
        var isChecked: Boolean? = flag
        val prefs = activity?.getSharedPreferences("DEBUG_SETTINGS",
                Context.MODE_PRIVATE)

        if (flag == null) {
            isChecked = prefs?.getBoolean("DRAWER", false)
        } else {
            with(prefs?.edit()) {
                this?.putBoolean("DRAWER", isChecked!!)
                this?.apply()
            }
        }
        listener.toggleDrawer(isChecked!!)
        debug_drawer.isChecked = isChecked
    }
    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume: $bottom_navigation")
    }
}
