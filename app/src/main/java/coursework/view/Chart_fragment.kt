package coursework.view

import coursework.Utils.stringToTimestamp
import coursework.Utils.timestampToDateString
import coursework.adapters.recyclers.ChartRecyclerAdapter
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.widget.TextView
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.jakewharton.rxbinding2.view.clicks
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_chart.*
import coursework.Utils.fetchColor
import coursework.model.AvailableCurrency
import coursework.model.HistoricalData
import coursework.viewModel.*
import javax.inject.Inject


class ChartFragment : AbstractFragment() {

    private val TAG: String = this.javaClass.simpleName
    private lateinit var chartViewModel: ChartViewModel
    @Inject
    lateinit var chartViewModelFactory: ChartViewModelFactory
    private lateinit var recyclerAdapter: ChartRecyclerAdapter
    private var mainCurrency: String = "RUB"
    private var baseCurrency: String = "EUR"
    private var selectionsCount: Int = 1
    private var isLongClicked: Boolean = false

    companion object {
        fun newInstance() = ChartFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        TITLE = activity?.getString(R.string.chart)?.toUpperCase() ?: super.TITLE
        chartViewModel = ViewModelProviders.of(this, chartViewModelFactory)
                .get(ChartViewModel::class.java)

        chartViewModel.historicalData().observe(this,
                Observer { it?.let { processHistory(it) } })
        chartViewModel.currencies().observe(this,
                Observer { it?.let { processCurrencies(it) } })
        chartViewModel.filter().observe(this,
                Observer { it?.let { processFilterChange(it) } })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chart, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerAdapter = ChartRecyclerAdapter(
                { i, _ -> onClick(i.sign) },
                { i, _ -> onLongClick(i.sign) })

        chart_recycler.adapter = recyclerAdapter
        chart_recycler.layoutManager = LinearLayoutManager(
                context, LinearLayoutManager.VERTICAL, false)


        chart_week.clicks()
                .subscribe {
                    chartViewModel.applyFilter(Filter.WEEK)
                    chartViewModel.loadHistoricalData(mainCurrency,
                            baseCurrency, Filter.WEEK)
                }
        chart_two_weeks.clicks()
                .subscribe {
                    chartViewModel.applyFilter(Filter.TWO_WEEKS)
                    chartViewModel.loadHistoricalData(mainCurrency,
                            baseCurrency, Filter.TWO_WEEKS)
                }
        chart_month.clicks()
                .subscribe {
                    chartViewModel.applyFilter(Filter.MONTH)
                    chartViewModel.loadHistoricalData(mainCurrency,
                            baseCurrency, Filter.MONTH)
                }
        chartViewModel.applyFilter(Filter.WEEK)
        animateFadeOut(chart_data_error, 0)
        chartViewModel.loadHistoricalData(mainCurrency, baseCurrency, chartViewModel.appliedFilter)
    }

    private fun processHistory(data: Response<List<HistoricalData>>) {
        when (data.status) {
            Status.LOADING -> {
                chart_loading.visibility = View.VISIBLE
            }
            Status.SUCCESS -> {
                drawChart(data.data)
            }
            Status.ERROR -> {
                Log.d(TAG, "historical data error")
                chart_loading.visibility = View.GONE
                animateFadeOut(chart_data_error, 3200)
            }
        }
    }

    private fun drawChart(data: List<HistoricalData>?) {
        val k = data?.sortedBy { it.date } ?: listOf()
        val entries: ArrayList<Entry> = arrayListOf()
        for (item in k) {
            val j = stringToTimestamp(item.date, "yyyy-MM-dd")

            var main = item.rates[mainCurrency]?.toFloat() ?: 0f
            var base = item.rates[baseCurrency]?.toFloat() ?: 0f

            val t = main
            main /= base
            base /= t
            //Log.d(TAG, "$index ${item.date} -  $m")
            entries.add(Entry(j.toFloat(), main))
        }
        val dataSet = LineDataSet(entries, "$mainCurrency relative to $baseCurrency  |  " +
                "N $mainCurrency for 1 $baseCurrency")
        val lineData = LineData(dataSet)
        chart.data = lineData

        dataSet.lineWidth = 4f
        dataSet.setDrawValues(false)
        dataSet.mode = LineDataSet.Mode.CUBIC_BEZIER
        val xAxis = chart.xAxis
        xAxis.position = XAxis.XAxisPosition.TOP
        xAxis.setDrawGridLines(false)
        xAxis.valueFormatter = IAxisValueFormatter { value, _ ->
            timestampToDateString(value.toLong(), "dd/MM")
        }
        xAxis.setCenterAxisLabels(true)
        val yAxis = chart.axisLeft
        chart.description.isEnabled = false
        yAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART)
        chart.axisRight.setCenterAxisLabels(true)
        chart.axisLeft.setCenterAxisLabels(true)
        chart.animateXY(200, 300,
                Easing.EasingOption.EaseInOutCubic, Easing.EasingOption.EaseInOutQuart)
        chart.invalidate()
        chart_loading.visibility = View.GONE
    }

    private fun processCurrencies(data: Response<List<AvailableCurrency>>) {
        when (data.status) {
            Status.LOADING -> {
            }
            Status.SUCCESS -> {
                chart_error.visibility = View.GONE
                recyclerAdapter.addItems(data.data!!, true)
                //chartViewModel.loadHistoricalData(mainCurrency, baseCurrency, chartViewModel.appliedFilter)
            }
            Status.ERROR -> {
                Log.d(TAG, data.error.toString())
                chart_error.visibility = View.VISIBLE
                filterDeselected(chartViewModel.appliedFilter)
            }
        }
    }

    //long click: first currency -> second click: base currency
    //click: first curr -> always EUR as a base currency
    fun onClick(item: String) {

        if (isLongClicked) {
            baseCurrency = item
        } else {
            mainCurrency = item
            baseCurrency = "EUR"
            selectionsCount = 0
        }
        isLongClicked = false
        filterDeselected(chartViewModel.appliedFilter)
    }

    private fun onLongClick(item: String) {
        selectionsCount = 1
        mainCurrency = item
        isLongClicked = true
        filterDeselected(chartViewModel.appliedFilter)
    }

    private fun animateFadeOut(v: View, t: Long) {
        val fadeOut = AlphaAnimation(1.0f, 0.0f)
        v.startAnimation(fadeOut)
        fadeOut.duration = t
        fadeOut.isFillEnabled = true
        fadeOut.fillAfter = true

        fadeOut.start()
    }

    private fun processFilterChange(filter: Response<FilterState>) {
        when (filter.status) {
            Status.LOADING -> {
            }
            Status.SUCCESS -> {
                Log.d(TAG, "${filter.data?.oldFilter?.ordinal}")
                filterDeselected(filter.data?.oldFilter)
                filterSelected(filter.data?.newFilter)
            }
            Status.ERROR -> {
            }
        }
    }

    private fun filterSelected(filter: Filter?) {
        val view = when (filter) {
            Filter.WEEK -> chart_week
            Filter.TWO_WEEKS -> chart_two_weeks
            Filter.MONTH -> chart_month
            else -> chart_week
        }

        view.cardElevation = 8.0f
        view.setCardBackgroundColor(fetchColor(context!!, R.color.fragment_2))
        (view.getChildAt(0) as TextView).setTextColor(fetchColor(context!!, R.color.white))
    }

    private fun filterDeselected(filter: Filter?) {
        val view = when (filter) {
            Filter.WEEK -> chart_week
            Filter.TWO_WEEKS -> chart_two_weeks
            Filter.MONTH -> chart_month
            else -> chart_week
        }

        view.cardElevation = 2.0f
        view.setCardBackgroundColor(fetchColor(context!!, R.color.white))
        (view.getChildAt(0) as TextView).setTextColor(fetchColor(context!!, R.color.text_subtle_2))
    }

    override fun onGainFocus() {
        super.onGainFocus()
        chartViewModel.loadCurrencies()
    }
}
