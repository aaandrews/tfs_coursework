package coursework.view

import coursework.Utils.fetchColor
import coursework.Utils.timestampToDateString
import coursework.adapters.recyclers.FilterRecyclerAdapter

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.CardView
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.View
import android.widget.TextView
import com.jakewharton.rxbinding2.view.clicks
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_history_filter.*
import coursework.model.OperationsHistory
import coursework.viewModel.*
import java.util.*
import javax.inject.Inject
import kotlin.collections.HashSet

class ActivityHistoryFilter : AppCompatActivity() {

    private val TAG: String = this.javaClass.simpleName
    private lateinit var recyclerAdapter: FilterRecyclerAdapter
    private lateinit var historyFilterViewModel: HistoryFilterViewModel
    @Inject
    lateinit var historyFilterViewModelFactory: HistoryFilterViewModelFactory
    private var filters = arrayListOf<CardView>()
    private var selections: HashSet<String> = hashSetOf()
    private var recreatedSelection: HashSet<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history_filter)
        AndroidInjection.inject(this)

        historyFilterViewModel = ViewModelProviders.of(this, historyFilterViewModelFactory).get(HistoryFilterViewModel::class.java)

        filters.addAll(listOf(filter_all_time, filter_week, filter_month, filter_date_range))
        recyclerAdapter = FilterRecyclerAdapter({ i, v -> onCurrencyClick(i, v) })
        val adapter = recyclerAdapter
        filter_recycler.adapter = adapter
        filter_recycler.layoutManager = GridLayoutManager(this, 5, GridLayoutManager.VERTICAL, false)

        historyFilterViewModel.history().observe(this, Observer { processHistory(it!!) })
        historyFilterViewModel.filter().observe(this, Observer { processFilterChange(it!!) })

        filter_week.clicks()
                .subscribe({ historyFilterViewModel.applyFilter(Filter.WEEK, this) })
        filter_month.clicks()
                .subscribe({ historyFilterViewModel.applyFilter(Filter.MONTH, this) })
        filter_all_time.clicks()
                .subscribe({ historyFilterViewModel.applyFilter(Filter.ALL_TIME, this) })
        filter_date_range.clicks()
                .subscribe({ historyFilterViewModel.applyFilter(Filter.RANGE, this) })

        apply_filter.clicks()
                .subscribe({
                    historyFilterViewModel.writeFilter(this, selections)
                    finish()
                })

        filter_select_all.clicks().subscribe({ selectAll() })
        filter_deselect_all.clicks().subscribe({ deselectAll() })

        if (savedInstanceState == null ||
                historyFilterViewModel.disposable.size() == 0) {
            historyFilterViewModel.loadHistory()
            historyFilterViewModel.loadFilter(this)
        }
        if (savedInstanceState != null) {
            recreatedSelection = HashSet(savedInstanceState.getStringArrayList("FILTERED_CURRENCIES"))
            Log.d(TAG, "${recreatedSelection?.size} $recreatedSelection")
            recreatedSelection?.let { selections.addAll(it) }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putStringArrayList("FILTERED_CURRENCIES", ArrayList(selections))
    }

    private fun processFilterChange(filter: Response<FilterState>) {
        when (filter.status) {
            Status.LOADING -> { }
            Status.SUCCESS -> {
                filterDeselected(filters[filter.data?.oldFilter?.ordinal!!])
                filterSelected(filters[filter.data.newFilter?.ordinal!!])

                val strF = timestampToDateString(filter.data.dateRangeFrom!!)
                val strT = timestampToDateString(filter.data.dateRangeTo!!)

                filter_date_range_text.text = "$strF  -  $strT"
            }
            Status.ERROR -> { }
        }
    }

    private fun filterSelected(view: CardView) {
        view.cardElevation = 8.0f
        view.setCardBackgroundColor(fetchColor(this, R.color.fragment_2))
        (view.getChildAt(0) as TextView).setTextColor(fetchColor(this, R.color.white))
    }

    private fun filterDeselected(view: CardView) {
        view.cardElevation = 2.0f
        view.setCardBackgroundColor(fetchColor(this, R.color.white))
        (view.getChildAt(0) as TextView).setTextColor(fetchColor(this, R.color.text_subtle_2))
    }

    private fun processHistory(data: Response<List<OperationsHistory>>) {
        when (data.status) {
            Status.LOADING -> { }
            Status.SUCCESS -> {
                val items: HashSet<String> = hashSetOf()
                for (i in data.data!!) {
                    items.add(i.base)
                    items.add(i.destination)
                }
                selections = recreatedSelection ?: HashSet(loadSelections())
                recyclerAdapter.addItems(items.toList(), true)
                recyclerAdapter.addSelections(selections)
            }
            Status.ERROR -> { }
        }
    }

    private fun selectAll() {
        recyclerAdapter.addSelections(HashSet(recyclerAdapter.elements))
        selections.addAll(recyclerAdapter.selections)
        recyclerAdapter.notifyDataSetChanged()
    }

    private fun deselectAll() {
        recyclerAdapter.removeSelections()
        selections = hashSetOf()
        recyclerAdapter.notifyDataSetChanged()
    }

    private fun onCurrencyClick(item: String, view: View) {
        if (selections.contains(item)) selections.remove(item)
        else selections.add(item)
    }

    private fun loadSelections(): Set<String> {
        val prefs = getSharedPreferences("FILTERS", Context.MODE_PRIVATE)
        return prefs.getStringSet("FILTERED_CURRENCIES", hashSetOf())
    }
}
