package coursework.view

import coursework.Utils.fetchDrawable
import coursework.Utils.roundToThree
import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_exchange.*
import coursework.model.AvailableCurrency
import coursework.model.ExchangeRates
import java.util.concurrent.TimeUnit
import android.support.design.widget.BottomSheetDialog
import android.view.View
import android.view.animation.AlphaAnimation
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.bottom_sheet_exchange.view.*
import coursework.viewModel.ExchangeViewModel
import coursework.viewModel.ExchangeViewModelFactory
import coursework.viewModel.Response
import coursework.viewModel.Status
import javax.inject.Inject


class ActivityExchange : AppCompatActivity() {
    private val TAG: String = this.javaClass.simpleName

    private lateinit var exchangeViewModel: ExchangeViewModel
    @Inject
    lateinit var exchangeViewModelFactory: ExchangeViewModelFactory
    private var fromCurrency: AvailableCurrency? = null
    private var toCurrency: AvailableCurrency? = null
    private var disposable = CompositeDisposable()
    private var rateFrom = 1.0
    private var rateTo = 1.0
    private var amountFrom: Double? = null
    private var amountTo: Double? = null
    private var dialog: BottomSheetDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exchange)
        AndroidInjection.inject(this)
        exchangeViewModel = ViewModelProviders.of(this, exchangeViewModelFactory).get(ExchangeViewModel::class.java)

        exchangeViewModel.exchangeRates().observe(this, Observer { processExchangeRates(it!!) })
        exchangeViewModel.exchangeRatesExpired().observe(this, Observer { processExpiredRate(it!!) })

        fromCurrency = intent.getParcelableExtra(ExchangeFragment.fromCurrBundleName)
        toCurrency = intent.getParcelableExtra(ExchangeFragment.toCurrBundleName)

        exchangeViewModel.loadExchangeRates()

        initView()
    }

    private fun initView() {
        exchange_button.isEnabled = false
        from_currency_name.text = fromCurrency?.name
        from_currency_sign.text = fromCurrency?.sign
        to_currency_sign.text = toCurrency?.sign
        to_currency_name.text = toCurrency?.name
        Log.d(TAG, "Init views")

        disposable.add(to_currency_amount.textChanges()
                .throttleLast(400, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    amountTo = if (it.toString().isNotEmpty()) it.toString().toDouble() else 0.0//amountTo
                    if (it.isNotEmpty() && to_currency_amount.isFocused)
                        exchangeToCurrencies(amountTo ?: 1.0)
                    Log.d(TAG, "amountTo: $amountTo")
                }
                .throttleLast(10, TimeUnit.SECONDS)
                .doOnNext {
                    Log.d(TAG, "Checking is expired")
                    exchangeViewModel.checkIsRateExpired()
                }
                .subscribe())

        disposable.add(from_currency_amount.textChanges()
                .throttleLast(400, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    amountFrom = if (it.toString().isNotEmpty()) it.toString().toDouble() else 0.0//amountFrom
                    if (it.isNotEmpty() && from_currency_amount.isFocused)
                        exchangeFromCurrencies(amountFrom ?: 1.0)
                    Log.d(TAG, "amountFrom : $amountFrom")
                }
                .throttleLast(10, TimeUnit.SECONDS)
                .doOnNext {
                    Log.d(TAG, "Checking is expired")
                    exchangeViewModel.checkIsRateExpired()
                }
                .subscribe())

        disposable.add(exchange_button.clicks()
                .subscribe({
                    if (amountTo == 0.0 || amountFrom == 0.0) {
                        errorChip(getString(R.string.empty_fields))
                    } else {
                        exchangeViewModel.checkIsRateExpired()
                        if (!exchangeViewModel.checkIsRateExpired()) {
                            exchangeButton()
                            finish()
                        } else {
                            exchangeViewModel.loadExchangeRates()
                            showBottomSheetDialog()
                        }
                    }
                }))
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

    @SuppressLint("SetTextI18n")
    private fun showBottomSheetDialog() {
        val view = layoutInflater.inflate(R.layout.bottom_sheet_exchange, null)

        dialog = BottomSheetDialog(this)
        dialog?.setContentView(view)
        dialog?.show()

        view.new_rate.text = "${fromCurrency?.sign}  $amountFrom   " +
                "-   $amountTo  ${toCurrency?.sign}"
        disposable.add(view.sheet_exchange.clicks()
                .subscribe({
                    exchangeButton()
                    dialog?.dismiss()
                    finish()
                }))
        disposable.add(view.sheet_cancel.clicks()
                .subscribe({
                    exchangeViewModel.loadExchangeRates()
                    dialog?.dismiss()
                }))
    }

    private fun processExpiredRate(exp: Boolean) {
        if (exp) {
            //call this from the activity rather than from the
            //view new_package.model directly because it must be called in
            //the ui thread
            exchangeViewModel.loadExchangeRates()
        }
    }

    private fun processExchangeRates(response: Response<ExchangeRates>?) {
        when (response?.status) {
            Status.LOADING -> {
                Log.d(TAG, "LOADING")
            }
            Status.SUCCESS -> {
                rateFrom = response.data?.rates?.get(toCurrency?.sign)?.toDouble()!!
                rateTo = response.data.rates[fromCurrency?.sign]?.toDouble()!!

                val t = rateTo
                rateTo /= rateFrom
                rateFrom /= t

                amountTo = amountTo ?: rateFrom
                amountFrom = amountFrom ?: rateTo

                exchangeFromCurrencies(amountFrom!!)
                //exchangeToCurrencies(amountTo!!)

                rates_updated.background = fetchDrawable(this, R.drawable.chip_grey)
                rates_updated.text = getString(R.string.success_exchange_rates)

                animateFadeOut(rates_updated)
                exchange_button.isEnabled = true

                Log.d(TAG, "SUCCESS: rates: ${response.data.rates[toCurrency?.sign]}  -  " +
                        "${response.data.rates[fromCurrency?.sign]}")
                Log.d(TAG, "amountFrom: $amountFrom, rateFrom: $rateFrom | " +
                        "amountTo: $amountTo, rateTo: $rateTo")
            }
            Status.ERROR -> {
                Log.d(TAG, response.error.toString())
                errorChip(getString(R.string.error_exchange_rates))
            }
        }
    }

    private fun exchangeFromCurrencies(from: Double) {
        val d = roundToThree(from * rateFrom)
        val dd = if (d % 1 > 0) d else d.toInt()

        Log.d(TAG, "setting from: $dd")
        to_currency_amount.setText(dd.toString(), TextView.BufferType.EDITABLE)

    }

    private fun exchangeToCurrencies(to: Double) {
        val d = roundToThree(to * rateTo)
        val dd = if (d % 1 > 0) d else d.toInt()

        Log.d(TAG, "setting to: $dd")
        from_currency_amount.setText(dd.toString(), TextView.BufferType.EDITABLE)
    }


    private fun exchangeButton(): Boolean {
        Log.d(TAG, "amountFrom: $amountFrom | amountTo: $amountTo")
        if (amountFrom == 0.0 || amountTo == 0.0) return false
        exchangeViewModel.writeOperationHistory(
                this,
                fromCurrency?.sign!!,
                toCurrency?.sign!!,
                from_currency_amount.text.toString().toDouble(),
                to_currency_amount.text.toString().toDouble())
        return true
    }

    private fun errorChip(error: String) {
        Log.d(TAG, "showing error chip")

        val an = AlphaAnimation(1.0f, 1.0f)
        rates_updated.startAnimation(an)
        an.duration = 0
        animateFadeOut(rates_updated)

        rates_updated.background = fetchDrawable(this, R.drawable.chip_error)
        rates_updated.text = error
    }

    private fun animateFadeOut(v: View) {
        val fadeOut = AlphaAnimation(1.0f, 0.0f)
        v.startAnimation(fadeOut)
        fadeOut.duration = 3200
        fadeOut.isFillEnabled = true
        fadeOut.fillAfter = true

        fadeOut.start()
    }
}
