package coursework.view

import coursework.Utils.fetchDrawable
import coursework.adapters.recyclers.ExchangeRecyclerAdapter
import coursework.adapters.ToolbarActionModeCallback
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.view.ActionMode
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_exchange.*
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import dagger.android.support.AndroidSupportInjection
import coursework.model.AvailableCurrency
import coursework.model.ExchangeRates
import coursework.viewModel.CurrenciesListViewModel
import coursework.viewModel.CurrenciesListViewModelFactory
import coursework.viewModel.Response
import coursework.viewModel.Status
import javax.inject.Inject
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import android.widget.GridLayout


abstract class AbstractFragment() : Fragment() {

    open var TITLE: String = ""
    open fun onLoseFocus() {}
    open fun onGainFocus() {}
    open fun getTitle(): String {
        return TITLE
    }

    open fun onBackPressed(): Boolean {
        return false
    }
}

class ExchangeFragment : AbstractFragment(), SwipeRefreshLayout.OnRefreshListener {

    private val TAG: String = this.javaClass.simpleName

    private var recyclerAdapter: ExchangeRecyclerAdapter? = null
    private var mActionMode: ActionMode? = null

    @Inject
    lateinit var exchangeViewModelFactory: CurrenciesListViewModelFactory

    private lateinit var exchangeViewModel: CurrenciesListViewModel
    private var isExchangeRequestError: Boolean = false

    private var fromCurrency: AvailableCurrency? = null
    private var toCurrency: AvailableCurrency? = null

    companion object {
        fun newInstance() = ExchangeFragment()

        const val fromCurrBundleName: String = "FROM_CURRENCY"
        const val toCurrBundleName: String = "TO_CURRENCY"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        TITLE = activity?.getString(R.string.exchange)?.toUpperCase() ?: super.TITLE

        exchangeViewModel = ViewModelProviders
                .of(this, exchangeViewModelFactory)
                .get(CurrenciesListViewModel::class.java)
        recyclerAdapter = ExchangeRecyclerAdapter(
                { item, view_ -> onclick(item, view_) },
                { item, _ -> onLongClick(item) })

        if (savedInstanceState == null) {
            exchangeViewModel.loadCurrencies()
            exchangeViewModel.loadExchangeRates()
        } else {
            val f: AvailableCurrency? = savedInstanceState.getParcelable(fromCurrBundleName)
            f?.let {
                Log.d(TAG, "before longclick")
                onLongClick(f)
                recyclerAdapter?.addSelections(listOf(fromCurrency, toCurrency))
                Log.d(TAG, "${recyclerAdapter?.hashCode()}")
            }
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        exchangeViewModel.currencies().observe(this, Observer { processCurrencies(it!!) })
        exchangeViewModel.exchangeRates().observe(this, Observer { processExchangeRates(it!!) })
        exchangeViewModel.timer().observe(this, Observer { timerText(it!!) })

        return inflater.inflate(R.layout.fragment_exchange, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        exchange_recycler.adapter = recyclerAdapter

        exchange_recycler.layoutManager = when (resources.configuration.orientation) {
            Configuration.ORIENTATION_PORTRAIT -> LinearLayoutManager(
                    context, LinearLayoutManager.VERTICAL, false)
            else -> GridLayoutManager(context, 3, LinearLayoutManager.VERTICAL,
                    false)
        }
        val resId = R.anim.layout_animation_fall_down
        val animation = AnimationUtils.loadLayoutAnimation(context, resId)
        exchange_recycler.layoutAnimation = animation
        swiperefresh.setOnRefreshListener(this)
    }

    override fun onStop() {
        super.onStop()
        fromCurrency = null
        toCurrency = null
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(fromCurrBundleName, fromCurrency)
    }

    private fun processCurrencies(data: Response<List<AvailableCurrency>>) {
        Log.d(TAG, "processing data ${data.status} ${data.data?.size} ${data.error}")
        when (data.status) {
            Status.LOADING -> {
                showLoading()
            }
            Status.SUCCESS -> {
                if (data.data?.size ?: 0 > 0) showSuccess()
                else showError()

                recyclerAdapter?.addItems(data.data!!, true)
            }
            Status.ERROR -> {
                showError()
            }
        }
    }

    private fun showLoading() {
        last_updated.visibility = View.GONE
        exchange_progress.visibility = View.VISIBLE

        exchange_error.visibility = View.GONE
        exchange_error_text.visibility = View.GONE
    }

    private fun showSuccess() {
        last_updated.visibility = View.VISIBLE
        exchange_progress.visibility = View.GONE

        exchange_error.visibility = View.GONE
        exchange_error_text.visibility = View.GONE
    }

    private fun showError() {
        exchange_progress.visibility = View.GONE
        last_updated.visibility = View.GONE

        exchange_error.visibility = View.VISIBLE
        exchange_error_text.visibility = View.VISIBLE
    }

    private fun processExchangeRates(data: Response<ExchangeRates>) {
        when (data.status) {
            Status.LOADING -> {
                swiperefresh.isRefreshing = true
            }
            Status.SUCCESS -> {
                swiperefresh.isRefreshing = false
            }
            Status.ERROR -> {
                Log.d(TAG, data.error.toString())
                swiperefresh.isRefreshing = false

                last_updated.background = fetchDrawable(this.context!!, R.drawable.chip_error)
                last_updated.text = getString(R.string.error_exchange_rates)
                isExchangeRequestError = true
            }
        }
    }

    private fun timerText(str: String) {
        if (isExchangeRequestError) {
            last_updated.background = fetchDrawable(this.context!!, R.drawable.chip_grey)
            isExchangeRequestError = false
        }
        last_updated.text = getString(R.string.last_updated, str)
    }

    private fun itemSelect() {

        if (mActionMode == null) {
            mActionMode = (activity as AppCompatActivity).startSupportActionMode(ToolbarActionModeCallback(recyclerAdapter!!, { onActionModeDestroy() }))
            mActionMode?.title = "FROM: ${fromCurrency?.sign}"
            mActionMode?.subtitle = "Choose the destination currency now"
        }


    }

    private fun onLongClick(item: AvailableCurrency?) {
        if (fromCurrency == null) {
            fromCurrency = item
        } else {
            toCurrency = item
            goExchangeActivity()
        }
        itemSelect()
    }

    private fun goExchangeActivity() {
        Intent(context, ActivityExchange::class.java).run {
            putExtra(fromCurrBundleName, fromCurrency)
            toCurrency = toCurrency ?: exchangeViewModel.getDestinationCurrency(fromCurrency?.sign!!)
            putExtra(toCurrBundleName, toCurrency)
            startActivity(this)
        }
        mActionMode?.finish()
    }

    fun onclick(item: AvailableCurrency, view: View) {
        when (view.id) {
            R.id.favorite -> {
                if (fromCurrency == null)
                    exchangeViewModel.setFavorite(item.id, !item.isFavorite)
            }
            else -> {
                if (fromCurrency != null) {
                    toCurrency = item
                } else {
                    fromCurrency = item
                }

                goExchangeActivity()
                recyclerAdapter?.removeAllSelections()
                mActionMode?.finish()
            }
        }
    }

    private fun onActionModeDestroy() {
        mActionMode = null
        fromCurrency = null
        toCurrency = null
    }

    override fun onLoseFocus() {
        Log.d(TAG, "onLoseFocus: selCount: ${recyclerAdapter?.getSelectionsCount()} | ${this}")
        if (recyclerAdapter?.getSelectionsCount() ?: 1 > 0) {
            recyclerAdapter?.removeAllSelections()
            mActionMode?.finish()
            fromCurrency = null
            toCurrency = null
        }
    }

    override fun onRefresh() {
        exchangeViewModel.loadExchangeRates()
    }
}
