package coursework.di

import coursework.Repository.CurrencyRepository
import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import coursework.model.CurrenciesDb
import javax.inject.Singleton
import coursework.App


@Module
class AppModule {

    @Provides
    fun bindApplication(app: App): Application = app

    @Provides
    @Singleton
    fun provideRepository(context: Context): CurrencyRepository {
        return CurrencyRepository(
                CurrenciesDb.getInstance(context).availableCurrenciesDao(),
                CurrenciesDb.getInstance(context).exchangeRatesDao(),
                CurrenciesDb.getInstance(context).operationsHistoryDao(),
                CurrenciesDb.getInstance(context).historicalDataDao())
    }

    @Provides
    @Singleton
    fun provideContext(application: Application): Context {
        return application
    }
}