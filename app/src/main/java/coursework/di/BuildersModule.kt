package coursework.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import coursework.view.*

@Module
abstract class BuildersModule {
    @ContributesAndroidInjector(modules = [(ExchangeModule::class)])
    abstract fun bindExchangeFragment(): ExchangeFragment

    @ContributesAndroidInjector(modules = [(MainActivityModule::class)])
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [(HistoryFragmentModule::class)])
    abstract fun bindHistoryFragment(): HistoryFragment

    @ContributesAndroidInjector(modules = [(ChartFragmentModule::class)])
    abstract fun bindChartFragment(): ChartFragment

    @ContributesAndroidInjector(modules = [(ExchangeActivityModule::class)])
    abstract fun bindActivityExchange(): ActivityExchange

    @ContributesAndroidInjector(modules = [(HistoryActivityModule::class)])
    abstract fun bindActivityHistoryFilter(): ActivityHistoryFilter
}
