package coursework.di


import dagger.Module
import dagger.Provides
import coursework.useCases.ChartUseCase
import coursework.useCases.CurrencyUseCase

import coursework.useCases.HistoryUseCase
import coursework.viewModel.*

@Module
class ExchangeModule {}

@Module
class MainActivityModule {}

@Module
class HistoryFragmentModule {
    @Provides
    fun provideHistoryViewModelFactory(loadHistoryUseCase: HistoryUseCase): HistoryViewModel {
        return HistoryViewModel(loadHistoryUseCase)
    }
}

@Module
class ChartFragmentModule {
    @Provides
    fun provideHistoryViewModelFactory(loadChartUseCase: ChartUseCase): ChartViewModel {
        return ChartViewModel(loadChartUseCase)
    }
}

@Module
class ExchangeActivityModule {
    @Provides
    fun provideExchangeActivityViewModelFactory(loadCurrencyUseCase: CurrencyUseCase): ExchangeViewModel {
        return ExchangeViewModel(loadCurrencyUseCase)
    }
}

@Module
class HistoryActivityModule {
    @Provides
    fun provideHistoryFilterViewModelFactory(loadHistoryUseCase: HistoryUseCase): HistoryFilterViewModel {
        return HistoryFilterViewModel(loadHistoryUseCase)
    }
}