package coursework.di

import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import coursework.App
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
        /* Use AndroidInjectionModule.class if you're not using support library */
        AndroidSupportInjectionModule::class,
        AppModule::class,
        MainActivityModule::class,
        HistoryFragmentModule::class,
        ChartFragmentModule::class,
        ExchangeActivityModule::class,
        HistoryActivityModule::class,
        BuildersModule::class))
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: App): Builder
        fun build(): AppComponent
    }

    fun inject(app: App)
}
