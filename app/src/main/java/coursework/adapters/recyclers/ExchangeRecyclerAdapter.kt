package coursework.adapters.recyclers

import coursework.Utils.fetchColor
import coursework.Utils.fetchDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import coursework.view.R
import kotlinx.android.synthetic.main.exchange_item.view.*
import coursework.model.AvailableCurrency

class ExchangeRecyclerAdapter(clickCB: (i: AvailableCurrency, v: View) -> Unit,
                              longClickCB: (i: AvailableCurrency, v: View) -> Unit) : RecyclerBaseAdapter<AvailableCurrency>(clickCB, longClickCB) {

    //private val TAG: String = this.javaClass.simpleName
    private var selectedItems: MutableSet<AvailableCurrency> = mutableSetOf()


    fun getSelections(): Set<AvailableCurrency> = selectedItems

    fun addSelections(l: List<AvailableCurrency?>) {
        val k = l.filterNotNull()
        selectedItems.addAll(k)
    }

    fun getSelectionsCount(): Int = selectedItems.size

    fun removeAllSelections() {
        selectedItems.clear()
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<AvailableCurrency> {
        val view = LayoutInflater.from(parent.context)
                .run { inflate(R.layout.exchange_item, parent, false) }
        return ExchangeViewHolder(view)
    }

    inner class ExchangeViewHolder(itemView: View) :
            BaseViewHolder<AvailableCurrency>(itemView),
            View.OnLongClickListener,
            View.OnClickListener {
        lateinit var item: AvailableCurrency
        override fun onClick(v: View) {
            when (v.id) {
                R.id.favorite -> clickCB(item, v)
                else -> {
                    if (selectedItems.add(item)) {
                        clickCB(item, v)
                    }
                }
            }
        }

        override fun onLongClick(v: View): Boolean {
            if (selectedItems.add(item)) {
                longClickCB(item, v)
                elements.indexOf(item).run {
                    if (this != -1) notifyItemChanged(this)
                }
            }
            return true
        }

        private fun checkIsSelected() {
            if (selectedItems.contains(this.item)) {
                itemView.exchange_currency_sign.setTextColor(fetchColor(itemView.context, R.color.white))
                itemView.exchange_currency_name.setTextColor(fetchColor(itemView.context, R.color.white))
                itemView.cardview.setCardBackgroundColor(fetchColor(itemView.context, R.color.colorAccent))
            } else {
                itemView.exchange_currency_sign.setTextColor(fetchColor(itemView.context, R.color.text_dark))
                itemView.exchange_currency_name.setTextColor(fetchColor(itemView.context, R.color.text_dark))
                itemView.cardview.setCardBackgroundColor(fetchColor(itemView.context, R.color.white))
            }
        }

        private fun checkIsFavorite() {
            if (!item.isFavorite) {
                itemView.favorite.setImageDrawable(fetchDrawable(
                        itemView.context, R.drawable.ic_star_border_grey_200_24dp))
            } else {
                itemView.favorite.setImageDrawable(fetchDrawable(
                        itemView.context, R.drawable.ic_star_yellow_400_24dp))
            }
        }

        override fun bind(item: AvailableCurrency, position: Int) {
            this.item = item
            itemView.exchange_currency_name.text = item.name
            itemView.exchange_currency_sign.text = item.sign
            itemView.setOnClickListener(this)
            itemView.favorite.setOnClickListener(this)
            itemView.setOnLongClickListener(this)

            checkIsSelected()
            checkIsFavorite()
        }
    }
}