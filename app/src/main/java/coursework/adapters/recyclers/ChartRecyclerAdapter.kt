package coursework.adapters.recyclers

import coursework.Utils.fetchColor
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import coursework.view.R
import kotlinx.android.synthetic.main.chart_item.view.*
import coursework.model.AvailableCurrency

class ChartRecyclerAdapter(clickCB: (item: AvailableCurrency, view: View) -> Unit = { _, _ -> },
                           longClickCB: (item: AvailableCurrency, view: View) -> Unit = { _, _ -> }) : RecyclerBaseAdapter<AvailableCurrency>(clickCB, longClickCB) {

    //private val TAG: String = this.javaClass.simpleName
    var selections: HashSet<AvailableCurrency> = hashSetOf()
    var isLongClicked: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<AvailableCurrency> {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.chart_item, parent, false)
        return ChartViewHolder(view)
    }

//    fun addSelections(selections: HashSet<AvailableCurrency>) {
//        this.selections.addAll(selections)
//    }

    inner class ChartViewHolder(itemView: View) : BaseViewHolder<AvailableCurrency>(itemView),
            View.OnClickListener,
            View.OnLongClickListener {
        var item: AvailableCurrency? = null
        private var pos: Int? = null
        override fun bind(item: AvailableCurrency, position: Int) {
            this.item = item
            this.pos = position
            itemView.chart_item_sign.text = item.sign
            itemView.chart_currency_name.text = item.name
            itemView.setOnClickListener(this)
            itemView.setOnLongClickListener(this)
            checkIfSelected()
        }

        private fun checkIfSelected() {
            if (selections.contains(item)) {
                itemView.chart_card_item.cardElevation = 5.0f
                itemView.chart_card_item.setCardBackgroundColor(
                        fetchColor(itemView.context, R.color.fragment_2))
                itemView.chart_item_sign.setTextColor(fetchColor(itemView.context, R.color.white))
            } else {
                itemView.chart_card_item.cardElevation = 2.0f
                itemView.chart_card_item.setCardBackgroundColor(fetchColor(itemView.context, R.color.white))
                itemView.chart_item_sign.setTextColor(fetchColor(itemView.context, R.color.text_subtle_2))
            }
        }

        override fun onLongClick(v: View?): Boolean {
            selections.clear()
            selections.add(item!!)
            isLongClicked = true
            checkIfSelected()
            notifyDataSetChanged()
            longClickCB(item!!, itemView)
            return true
        }

        override fun onClick(v: View?) {
            if (isLongClicked) {
                selections.add(item!!)
            } else {
                selections.clear()
                selections.add(item!!)
            }
            isLongClicked = false
            checkIfSelected()
            notifyDataSetChanged()
            clickCB(item!!, itemView)
        }
    }
}