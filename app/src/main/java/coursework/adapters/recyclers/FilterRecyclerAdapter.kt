package coursework.adapters.recyclers

import coursework.Utils.fetchColor
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import coursework.view.R
import kotlinx.android.synthetic.main.filter_item.view.*


class FilterRecyclerAdapter(clickCB: (item: String, view: View) -> Unit = { _, _ -> },
                            longClickCB: (item: String, view: View) -> Unit = { _, _ -> }) :
        RecyclerBaseAdapter<String>(clickCB, longClickCB) {

    var selections: HashSet<String> = hashSetOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<String> {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.filter_item, parent, false)
        return FilterViewHolder(view)
    }

    fun addSelections(selections: HashSet<String>) {
        this.selections.addAll(selections)
    }

    fun removeSelections() {
        this.selections = hashSetOf()
    }

    inner class FilterViewHolder(itemView: View) : BaseViewHolder<String>(itemView), View.OnClickListener {
        var item: String? = null

        override fun bind(item: String, position: Int) {
            this.item = item
            itemView.filter_item_sign.text = item
            itemView.setOnClickListener(this)

            checkIfSelected()
        }

        private fun checkIfSelected() {
            if (selections.contains(item)) {
                itemView.filter_card_item.cardElevation = 5.0f
                itemView.filter_card_item.setCardBackgroundColor(fetchColor(
                        itemView.context, R.color.fragment_3))
                itemView.filter_item_sign.setTextColor(fetchColor(itemView.context, R.color.white))
            } else {
                itemView.filter_card_item.cardElevation = 2.0f
                itemView.filter_card_item.setCardBackgroundColor(fetchColor(itemView.context, R.color.white))
                itemView.filter_item_sign.setTextColor(fetchColor(itemView.context, R.color.text_subtle_2))
            }
        }

        override fun onClick(v: View?) {
            if (selections.contains(item)) {
                selections.remove(item)
            } else {
                selections.add(item!!)
            }
            clickCB(item!!, itemView)
            checkIfSelected()
        }
    }
}