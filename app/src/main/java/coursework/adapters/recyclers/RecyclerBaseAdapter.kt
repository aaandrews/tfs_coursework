package coursework.adapters.recyclers


import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.View

abstract class RecyclerBaseAdapter<T>(var clickCB: (i: T, v: View) -> Unit,
                                      var longClickCB: (i: T, v: View) -> Unit)
    : RecyclerView.Adapter<RecyclerBaseAdapter<T>.BaseViewHolder<T>>() {

    open var elements: ArrayList<T> = arrayListOf()

    fun getItems(): ArrayList<T> = ArrayList(elements)

    open fun addItems(items: List<T>, replace: Boolean) {
        //val result = DiffUtil.calculateDiff(DiffUtil(elements, items))
        var start = if (this.elements.size == 0) 0 else this.elements.size - 1
        if (replace) {
            this.elements.clear()
            this.elements = ArrayList(items)
            start = 0
            notifyDataSetChanged()
        } else {
            this.elements.addAll(items)
            notifyItemRangeInserted(start, (this.elements.size - 1))
        }
        //result.dispatchUpdatesTo(this)
    }

    fun addItem(item: T) {
        elements.add(item)
        notifyItemInserted(elements.size - 1)
    }

    override fun getItemCount(): Int = elements.size

    override fun onBindViewHolder(holder: BaseViewHolder<T>, position: Int) {
        holder.bind(elements[position], position)
    }

    abstract inner class BaseViewHolder<in T>(itemView: View) :
            RecyclerView.ViewHolder(itemView) {
        abstract fun bind(item: T, position: Int)
    }
}
