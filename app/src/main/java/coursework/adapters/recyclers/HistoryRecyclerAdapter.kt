package coursework.adapters.recyclers

import coursework.Utils.format
import coursework.Utils.takeAndAppend
import coursework.Utils.timestampToDateString
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import coursework.view.R
import kotlinx.android.synthetic.main.history_item.view.*
import coursework.model.AvailableCurrency
import coursework.model.OperationsHistory

class HistoryRecyclerAdapter(clickCB: (item: Any, view: View) -> Unit = { _, _ -> },
                             longClickCB: (item: Any, view: View) -> Unit = { _, _ -> }) :
        RecyclerBaseAdapter<OperationsHistory>(clickCB, longClickCB) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<OperationsHistory> {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.history_item, parent, false)
        return HistoryViewHolder(view)
    }

    inner class HistoryViewHolder(itemView: View) : BaseViewHolder<OperationsHistory>(itemView) {
        override fun bind(item: OperationsHistory, position: Int) {
            itemView.history_from_currency.text = item.base
            itemView.history_to_currency.text = item.destination
            itemView.history_from_amount.text = item.baseAmount
                    .format(2)
                    .takeAndAppend(9, "...")
            itemView.history_to_amount.text = item.destinationAmount
                    .format(2)
                    .takeAndAppend(9, "...")
            itemView.history_operation_datetime.text = timestampToDateString(item.timestamp * 1000)
        }
    }
}