package coursework.adapters

import android.content.Context
import android.support.design.widget.BottomNavigationView
import android.support.v4.view.ViewCompat
import android.support.design.widget.CoordinatorLayout
import android.view.View


class BottomNavigationViewBehavior(var context: Context) : CoordinatorLayout.Behavior<BottomNavigationView>() {

    //private val TAG: String = this.javaClass.simpleName
    private var bottomNav: BottomNavigationView? = null
    private var height: Int = 0
    private var isHidden: Boolean = false

    override fun onLayoutChild(parent: CoordinatorLayout?,
                               child: BottomNavigationView,
                               layoutDirection: Int): Boolean {
        bottomNav = child
        height = child.height

        return super.onLayoutChild(parent, child, layoutDirection)
    }


    override fun onStartNestedScroll(coordinatorLayout: CoordinatorLayout,
                                     child: BottomNavigationView,
                                     directTargetChild: View, target: View,
                                     axes: Int, type: Int): Boolean {
        return axes == ViewCompat.SCROLL_AXIS_VERTICAL
    }

    override fun onNestedScroll(coordinatorLayout: CoordinatorLayout,
                                child: BottomNavigationView,
                                target: View, dxConsumed: Int, dyConsumed: Int,
                                dxUnconsumed: Int, dyUnconsumed: Int,
                                @ViewCompat.NestedScrollType type: Int) {
        if (dyConsumed > 0 && !isHidden) {
            slideDown()
            isHidden = true
        } else if (dyConsumed < 0 && isHidden) {
            slideUp()
            isHidden = false
        }
    }

    fun slideUp() {
        bottomNav?.clearAnimation()
        bottomNav?.apply { animate().translationY(0f).duration = 350 }
    }

    fun slideDown() {
        bottomNav?.clearAnimation()
        bottomNav?.apply { animate().translationY(height.toFloat()).duration = 350 }
    }

}