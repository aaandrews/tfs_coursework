package coursework.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.PagerAdapter
import android.util.Log
import android.view.ViewGroup
import coursework.view.*


class ViewpagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    private val TAG: String = javaClass.simpleName
    private var fragments: ArrayList<AbstractFragment> = arrayListOf()
    var prevFragment: Int = 0

    override fun getCount(): Int = fragments.size

    fun getFragment(pos: Int): AbstractFragment {
        Log.d(TAG, "getPrevFragment: $pos | ${fragments[pos]}")
        return fragments[pos]
    }

    fun addFragment(frag: AbstractFragment) {
        fragments.add(frag)
        if (fragments.size == 1) prevFragment = fragments.size - 1
        notifyDataSetChanged()

    }

    fun addFragments(frags: List<AbstractFragment>) {
        fragments = ArrayList(frags)
        notifyDataSetChanged()
    }

    // Returns the fragment to display for that page
    override fun getItem(position: Int): Fragment = fragments[position]

    // Returns the page title for the top indicator
    override fun getPageTitle(position: Int): CharSequence? =
            fragments[position].getTitle()

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val ret: AbstractFragment = super.instantiateItem(container, position) as AbstractFragment
        fragments[position] = ret
        Log.d(TAG, "pos: $position | frag: $ret")
        return ret
    }
}
