package coursework.adapters

import coursework.adapters.recyclers.ExchangeRecyclerAdapter
import android.support.v7.view.ActionMode
import android.view.Menu
import android.view.MenuItem
import coursework.view.R


class ToolbarActionModeCallback(private val recyclerView_adapter: ExchangeRecyclerAdapter,
                                private var destroyCB: () -> Unit = {}) : ActionMode.Callback {

    override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
        mode.menuInflater.inflate(R.menu.exchange_actionmode, menu)//Inflate the menu over action mode
        return true
    }

    override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
        return true
    }

    override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
        mode.finish()
        return false
    }


    override fun onDestroyActionMode(mode: ActionMode) {
        recyclerView_adapter.removeAllSelections()
        destroyCB()
    }
}