package coursework.viewModel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import coursework.model.AvailableCurrency
import coursework.model.ExchangeRates
import coursework.useCases.CurrencyUseCase
import java.util.concurrent.TimeUnit

class CurrenciesListViewModel(private var loadCurrencyUseCase: CurrencyUseCase
) : ViewModel() {

    private val TAG: String = this.javaClass.simpleName
    private var response: MutableLiveData<Response<List<AvailableCurrency>>> = MutableLiveData()
    private var exchangeResponse: MutableLiveData<Response<ExchangeRates>> = MutableLiveData()
    private var timerRefreshed: MutableLiveData<String> = MutableLiveData()
    private var timerDisposable: Disposable? = null
    private var disposable: CompositeDisposable = CompositeDisposable()
    private var currenciesListError: Boolean = false

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
        timerDisposable?.dispose()
        Log.d(TAG, "cleared")
    }


    fun currencies(): LiveData<Response<List<AvailableCurrency>>> {
        return response
    }

    fun exchangeRates(): LiveData<Response<ExchangeRates>> {
        return exchangeResponse
    }

    fun timer(): LiveData<String> {
        return timerRefreshed
    }

    fun setFavorite(id: Long, fav: Boolean) {
        loadCurrencyUseCase.setFavorite(id, fav)
    }

    private fun startTimer() {
        timerDisposable?.dispose()
        timerDisposable = Observable.interval(1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    timerRefreshed.value = it.toString()
                })
    }

    fun loadCurrencies() {
        disposable.add(loadCurrencyUseCase.loadCurrencies()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe {
                    response.value = Response.loading()
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Log.d(TAG, "data success: ${it.size}")
                    response.value = Response.success(it)
                    currenciesListError = false
                }, {
                    Log.d(TAG, "data error")
                    currenciesListError = true
                    response.value = Response.error(it)
                })
        )
    }

    fun loadExchangeRates() {
        if (currenciesListError) {
            Log.d(TAG, "load currency list after error")
            loadCurrencies()
        }
        loadCurrencyUseCase.exchangeRate()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe {
                    exchangeResponse.value = Response.loading()
                    Log.d(TAG, "loading exchange rates: ${exchangeResponse.hasObservers()} ${exchangeResponse.hasActiveObservers()}")
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    exchangeResponse.value = Response.success(it)
                    timerRefreshed.value = "just now"
                    Log.d(TAG, "exchange_rates success ${exchangeResponse.hasObservers()} ${exchangeResponse.hasActiveObservers()} ${exchangeResponse.value}")
                    startTimer()
                }, {
                    timerDisposable?.dispose()
                    exchangeResponse.value = Response.error(it)
                })
    }

    fun getDestinationCurrency(not: String): AvailableCurrency {
        val favorites: List<AvailableCurrency>? = response
                .value?.data?.filter { it.isFavorite && it.sign != not }

        return favorites?.run {
            if (this.isNotEmpty()) this[0] else null
        } ?: response.value?.data?.find { it.sign == "RUB" }
        ?: response.value?.data?.find { it.sign == "USD" }
        ?: response.value?.data?.first { it.sign != not }!!
    }

}
