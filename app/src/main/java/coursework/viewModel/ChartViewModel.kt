package coursework.viewModel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import coursework.model.AvailableCurrency
import coursework.model.HistoricalData
import coursework.useCases.ChartUseCase
import java.util.concurrent.TimeUnit

class ChartViewModel(private var chartUseCase: ChartUseCase) : ViewModel() {

    private var TAG: String = this.javaClass.simpleName

    private var chartResponse: MutableLiveData<Response<List<HistoricalData>>> = MutableLiveData()
    private var currenciesResponse: MutableLiveData<Response<List<AvailableCurrency>>> = MutableLiveData()
    private var filterState: MutableLiveData<Response<FilterState>> = MutableLiveData()
    private var disposable: CompositeDisposable = CompositeDisposable()
    var appliedFilter: Filter = Filter.WEEK

    init { chartUseCase.removeOldHistoricalData() }
    override fun onCleared() {
        super.onCleared()
        if (!disposable.isDisposed) disposable.clear()
    }

    fun historicalData(): LiveData<Response<List<HistoricalData>>> = chartResponse
    fun filter(): LiveData<Response<FilterState>> = filterState
    fun currencies(): LiveData<Response<List<AvailableCurrency>>> = currenciesResponse

    fun loadHistoricalData(curr_one: String, curr_two: String? = null, range: Filter) {
        //disposable.clear()
        disposable.add(
                chartUseCase.loadHistoricalRange(curr_one, curr_two, range)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe { chartResponse.value = Response.loading() }
                        .subscribe(
                                { chartResponse.value = Response.success(it) },
                                { chartResponse.value = Response.error(it) }))
    }

    fun loadCurrencies() {
        disposable.add(chartUseCase.loadCurrencies()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { currenciesResponse.value = Response.loading() }
                .subscribe(
                        { currenciesResponse.value = Response.success(it) },
                        { currenciesResponse.value = Response.error(it) }))
    }

    fun applyFilter(f: Filter) {
        val oldFilter = appliedFilter
        appliedFilter = f
        filterState.value = Response.success(FilterState(oldFilter, appliedFilter))
    }
}