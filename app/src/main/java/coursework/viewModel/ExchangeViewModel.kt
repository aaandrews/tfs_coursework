package coursework.viewModel

import coursework.Repository.CurrencyRepository
import android.app.Activity
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import coursework.model.ExchangeRates
import coursework.useCases.CurrencyUseCase


class ExchangeViewModel(private var loadCurrencyUseCase: CurrencyUseCase
) : ViewModel() {
    private val TAG: String = this.javaClass.simpleName
    private var exchangeResponse: MutableLiveData<Response<ExchangeRates>> = MutableLiveData()
    private var isExchangeRateExpired: MutableLiveData<Boolean> = MutableLiveData()
    private var disposable: CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
        Log.d(TAG, "cleared")
    }

    fun exchangeRates(): LiveData<Response<ExchangeRates>> {
        return exchangeResponse
    }

    fun exchangeRatesExpired() : LiveData<Boolean> {
        return isExchangeRateExpired
    }


    fun loadExchangeRates() {
        loadCurrencyUseCase.exchangeRate()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe {
                    exchangeResponse.value = Response.loading()
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    exchangeResponse.value = Response.success(it)
                }, {
                    exchangeResponse.value = Response.error(it)
                })
    }

    fun writeOperationHistory(context: Context, base: String, dest: String,
                              baseAmount: Double, destAmount: Double) {

        loadCurrencyUseCase.writeOperationHistory(base, dest, baseAmount, destAmount)

        //automatically select/add from/to currencies to the historyFilter prefs
        val prefs = (context as Activity)
                .getSharedPreferences("FILTERS", Context.MODE_PRIVATE)
                ?: return
        val currencies = prefs.getStringSet("FILTERED_CURRENCIES", hashSetOf())
        currencies.addAll(listOf(base, dest))
        with(prefs.edit()) {
            putStringSet("FILTERED_CURRENCIES", currencies)
            apply()
        }
    }

    fun checkIsRateExpired() : Boolean{
        val fetched = exchangeResponse.value?.data?.timestamp_fetched
        val now = System.currentTimeMillis() / 1000
        val diff = (now - (fetched ?: 0)) > CurrencyRepository.EXCHANGE_RATES_CACHE_TIME
        Log.d(TAG, "$diff $now $fetched")
        if(diff) {
            isExchangeRateExpired.postValue(true)
            return true
        }
        return false
    }
}