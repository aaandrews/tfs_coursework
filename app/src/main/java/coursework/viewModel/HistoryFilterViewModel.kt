package coursework.viewModel

import coursework.Utils.dateToCalendar
import coursework.Utils.stringToTimestamp
import android.app.Activity
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.util.Log
import com.borax12.materialdaterangepicker.date.DatePickerDialog
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import coursework.model.OperationsHistory
import coursework.useCases.HistoryUseCase
import java.util.*
import kotlin.collections.HashSet


class HistoryFilterViewModel(var historyUseCase: HistoryUseCase
) : ViewModel(), DatePickerDialog.OnDateSetListener {
    private var TAG: String = this.javaClass.simpleName
    private var historyResponse: MutableLiveData<Response<List<OperationsHistory>>> = MutableLiveData()
    private var filterState: MutableLiveData<Response<FilterState>> = MutableLiveData()
    var disposable: CompositeDisposable = CompositeDisposable()
    private var appliedFilter: Filter = Filter.MONTH
    private var dateFrom: Long = Date().time
    private var dateTo: Long = Date().time

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
        Log.d(TAG, "cleared")
    }

    fun history(): LiveData<Response<List<OperationsHistory>>> = historyResponse

    fun filter(): LiveData<Response<FilterState>> = filterState

    fun loadHistory() {
        disposable.add(historyUseCase.allHistory()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe {
                    historyResponse.value = Response.loading()
                }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext { Log.d(TAG, "on next") }
                .subscribe({
                    historyResponse.value = Response.success(it)
                }, {
                    historyResponse.value = Response.error(it)
                })
        )
    }

    fun loadFilter(context: Context) {
        val oldFilter = appliedFilter
        val prefs = (context as Activity).getSharedPreferences("FILTERS", Context.MODE_PRIVATE)
                ?: return
        appliedFilter = Filter.values().first { it.ordinal == prefs.getInt("APPLIED_FILTER", 0) }
        dateFrom = prefs.getLong("DATE_FROM", dateFrom)
        dateTo = prefs.getLong("DATE_TO", dateTo)

        filterState.value = Response.success(FilterState(oldFilter, appliedFilter, dateFrom, dateTo))
    }

    fun writeFilter(context: Context, currencies: HashSet<String>) {
        val prefs = (context as Activity)
                .getSharedPreferences("FILTERS", Context.MODE_PRIVATE)
                ?: return
        with(prefs.edit()) {
            putInt("APPLIED_FILTER", appliedFilter.ordinal)
            putStringSet("FILTERED_CURRENCIES", currencies)
            putLong("DATE_FROM", dateFrom)
            putLong("DATE_TO", dateTo)
            apply()
        }
    }

    fun applyFilter(f: Filter, context: Context) {
        when (f) {
            Filter.ALL_TIME,
            Filter.WEEK,
            Filter.MONTH -> {
                val oldFilter = appliedFilter
                appliedFilter = f
                filterState.value = Response.success(FilterState(oldFilter, appliedFilter, dateFrom, dateTo))
            }
            Filter.RANGE -> {
                showRange(context)
            }
            else -> { }
        }
    }

    private fun showRange(context: Context) {
        val now = Calendar.getInstance()
        val then = dateToCalendar(dateFrom)
        val dpd = DatePickerDialog.newInstance(
                this,
                then.get(Calendar.YEAR),
                then.get(Calendar.MONTH),
                then.get(Calendar.DAY_OF_MONTH),
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        )
        dpd.show((context as Activity).fragmentManager, "Datepickerdialog")
    }

    override fun onDateSet(view: DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int, yearEnd: Int, monthOfYearEnd: Int, dayOfMonthEnd: Int) {
        Log.d("HISTORY_MODEL", "one data set picker callback")
        val monthOfYearH = monthOfYear + 1
        val monthOfYearEndH = monthOfYearEnd + 1

        dateFrom = stringToTimestamp("$dayOfMonth/$monthOfYearH/$year")
        dateTo = stringToTimestamp("$dayOfMonthEnd/$monthOfYearEndH/$yearEnd", dayEnd = true)

        val oldFilter = appliedFilter
        appliedFilter = Filter.RANGE
        filterState.value = Response.success(FilterState(oldFilter, appliedFilter, dateFrom, dateTo))
    }
}

data class FilterState(var oldFilter: Filter?,
                       var newFilter: Filter?,
                       var dateRangeFrom: Long? = null,
                       var dateRangeTo: Long? = null)

enum class Filter(i: Int) {
    ALL_TIME(0),
    WEEK(1),
    MONTH(2),
    RANGE(3),
    TWO_WEEKS(4) }