package coursework.viewModel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.content.Intent
import android.util.Log
import coursework.view.ActivityHistoryFilter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import coursework.model.OperationsHistory
import coursework.useCases.HistoryUseCase

class HistoryViewModel(var historyUseCase: HistoryUseCase
) : ViewModel() {
    private var TAG : String = this.javaClass.simpleName
    private var historyResponse: MutableLiveData<Response<List<OperationsHistory>>> = MutableLiveData()
    private var disposable: CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
        Log.d(TAG, "cleared")
    }

    fun history(): LiveData<Response<List<OperationsHistory>>> = historyResponse

    fun loadHistory() {
        disposable.add(historyUseCase.allHistory()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe {
                    historyResponse.value = Response.loading()
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    historyResponse.value = Response.success(it)
                }, {
                    historyResponse.value = Response.error(it)
                })
        )
    }

    fun openFilters(context: Context) {
        context.startActivity(Intent(context, ActivityHistoryFilter::class.java))
    }
}