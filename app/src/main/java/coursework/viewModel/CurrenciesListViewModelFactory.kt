package coursework.viewModel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import coursework.useCases.ChartUseCase
import coursework.useCases.CurrencyUseCase
import coursework.useCases.HistoryUseCase
import javax.inject.Inject


class CurrenciesListViewModelFactory @Inject constructor(var loadCurrencyUseCase: CurrencyUseCase) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CurrenciesListViewModel::class.java)) {
            return CurrenciesListViewModel(loadCurrencyUseCase) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

class ExchangeViewModelFactory @Inject constructor(var loadCurrencyUseCase: CurrencyUseCase) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ExchangeViewModel::class.java)) {
            return ExchangeViewModel(loadCurrencyUseCase) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

class HistoryViewModelFactory @Inject constructor(var historyUseCase: HistoryUseCase) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(HistoryViewModel::class.java)) {
            return HistoryViewModel(historyUseCase) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

class HistoryFilterViewModelFactory @Inject constructor(var historyUseCase: HistoryUseCase) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(HistoryFilterViewModel::class.java)) {
            return HistoryFilterViewModel(historyUseCase) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

class ChartViewModelFactory @Inject constructor(var chartUseCase: ChartUseCase) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ChartViewModel::class.java)) {
            return ChartViewModel(chartUseCase) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}