package coursework.viewModel

import coursework.viewModel.Status.*


class Response<T> private constructor(val status: Status,
                                      val data: T?,
                                      val error: Throwable?) {


    override fun toString(): String {
        return "Resource{" +
                "status=" + status +
                ", message='" + error?.message + '\''.toString() +
                ", data=" + data +
                '}'.toString()
    }

    companion object {

        fun <T> success(data: T): Response<T> {
            return Response(SUCCESS, data, null)
        }

        fun <T> error(err: Throwable): Response<T> {
            return Response(ERROR, null, err)
        }

        fun <T> loading(): Response<T> {
            return Response(LOADING, null, null)
        }
    }
}

enum class Status {
    LOADING,
    SUCCESS,
    ERROR
}