# tfs_coursework

### TODO

* Tests
* Profiling
* Inspect memory usage with LeakCannary
* Inspect layout overdraws
* Implement internet connection warnings
* Implement history item removal(one by one, by swipe)

### Known Bugs
* swipe from frag_one to frag_two after orientation change(View pager problem)
